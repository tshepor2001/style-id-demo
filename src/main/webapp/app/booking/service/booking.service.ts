import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { Pagination } from 'app/core/request/request.model';
import { Desk } from '../desk/model/desk.model';

@Injectable({ providedIn: 'root' })
export class BookingService {
  private resourceUrl = this.applicationConfigService.getEndpointFor('api/desks');

  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  query(req?: Pagination): Observable<HttpResponse<Desk[]>> {
    const options = createRequestOption(req);
    return this.http.get<Desk[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  authorities(): Observable<string[]> {
    return this.http.get<string[]>(this.applicationConfigService.getEndpointFor('api/authorities'));
  }
}
