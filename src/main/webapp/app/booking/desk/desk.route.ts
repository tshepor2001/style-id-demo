import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DeskComponent } from './desk.component';

export const deskRoute: Route = {
  path: 'desks',
  component: DeskComponent,
  data: {
    pageTitle: 'global.menu.booking.desks',
    defaultSort: 'id,asc',
  },
  canActivate: [UserRouteAccessService],
};
