export interface Desk {
  id: number;
  name: string;
  monitors: number;
  dockingStationType: DockingStationType;
}

export interface DockingStationType {
  id: number;
  name: string;
}
