import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { DeskComponent } from './desk/desk.component';
import { bookingState } from './booking.route';

@NgModule({
  imports: [SharedModule, RouterModule.forChild(bookingState)],
  declarations: [DeskComponent],
})
export class BookingModule {}
