import { Routes } from '@angular/router';

import { deskRoute } from './desk/desk.route';

const BOOKING_ROUTES = [deskRoute];

export const bookingState: Routes = [
  {
    path: '',
    children: BOOKING_ROUTES,
  },
];
