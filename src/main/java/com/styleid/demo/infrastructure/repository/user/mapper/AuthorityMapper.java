package com.styleid.demo.infrastructure.repository.user.mapper;

import com.styleid.demo.domain.aggregate.user.model.Authority;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AuthorityMapper {
    AuthorityMapper INSTANCE = Mappers.getMapper(AuthorityMapper.class);

    Authority toDomain(com.styleid.demo.infrastructure.repository.user.model.Authority authority);
    com.styleid.demo.infrastructure.repository.user.model.Authority fromDomain(Authority authority);
}
