package com.styleid.demo.infrastructure.repository.booking;

import com.styleid.demo.infrastructure.repository.booking.model.Desk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeskRepository extends JpaRepository<Desk, Long> {}
