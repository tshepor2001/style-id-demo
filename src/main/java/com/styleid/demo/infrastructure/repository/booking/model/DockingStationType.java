package com.styleid.demo.infrastructure.repository.booking.model;

import javax.persistence.*;
import lombok.*;

@Entity
@Table(name = "jhi_docking_station_type")
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DockingStationType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true, nullable = false)
    private String name;
}
