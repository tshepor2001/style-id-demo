package com.styleid.demo.infrastructure.repository.user.mapper;

import com.styleid.demo.domain.aggregate.user.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { AuthorityMapper.class })
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toDomain(com.styleid.demo.infrastructure.repository.user.model.User user);
    com.styleid.demo.infrastructure.repository.user.model.User fromDomain(User user);
}
