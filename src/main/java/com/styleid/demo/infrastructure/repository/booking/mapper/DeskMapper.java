package com.styleid.demo.infrastructure.repository.booking.mapper;

import com.styleid.demo.domain.aggregate.booking.desk.model.Desk;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DeskMapper {
    DeskMapper INSTANCE = Mappers.getMapper(DeskMapper.class);

    Desk toDomain(com.styleid.demo.infrastructure.repository.booking.model.Desk desk);
    com.styleid.demo.infrastructure.repository.booking.model.Desk fromDomain(Desk desk);
}
