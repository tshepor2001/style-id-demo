package com.styleid.demo.infrastructure.repository.user;

import com.styleid.demo.infrastructure.repository.user.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, String> {}
