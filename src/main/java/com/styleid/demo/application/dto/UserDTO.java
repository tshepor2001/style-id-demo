package com.styleid.demo.application.dto;

import com.styleid.demo.domain.aggregate.user.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class UserDTO {

    private Long id;
    private String login;

    public UserDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
    }
}
