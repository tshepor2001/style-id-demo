package com.styleid.demo.application.web.rest;

import com.styleid.demo.application.dto.UserDTO;
import com.styleid.demo.domain.aggregate.booking.desk.model.Desk;
import com.styleid.demo.domain.aggregate.booking.service.BookingService;
import com.styleid.demo.domain.aggregate.user.service.UserService;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class PublicBookingResource {

    private static final List<String> ALLOWED_ORDERED_PROPERTIES = Collections.unmodifiableList(
        Arrays.asList("id", "name", "monitors", "dockingStationType")
    );

    private final Logger log = LoggerFactory.getLogger(PublicBookingResource.class);

    private BookingService bookingService;

    public PublicBookingResource(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping("/desks")
    public ResponseEntity<List<Desk>> getDesks(Pageable pageable) {
        if (!onlyContainsAllowedProperties(pageable)) {
            return ResponseEntity.badRequest().build();
        }
        Page<Desk> page = bookingService.getAllDesks(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private boolean onlyContainsAllowedProperties(Pageable pageable) {
        return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
    }
}
