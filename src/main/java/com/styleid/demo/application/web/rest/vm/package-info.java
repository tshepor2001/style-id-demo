/**
 * View Models used by Spring MVC REST controllers.
 */
package com.styleid.demo.application.web.rest.vm;
