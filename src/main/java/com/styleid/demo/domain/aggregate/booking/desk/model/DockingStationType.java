package com.styleid.demo.domain.aggregate.booking.desk.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.*;

@Getter
@EqualsAndHashCode
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DockingStationType {

    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;
}
