package com.styleid.demo.domain.aggregate.booking.service;

import com.styleid.demo.domain.aggregate.booking.desk.model.Desk;
import com.styleid.demo.infrastructure.repository.booking.DeskRepository;
import com.styleid.demo.infrastructure.repository.booking.mapper.DeskMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BookingService {

    private final Logger log = LoggerFactory.getLogger(BookingService.class);

    private final DeskRepository deskRepository;

    public BookingService(DeskRepository deskRepository) {
        this.deskRepository = deskRepository;
    }

    public Page<Desk> getAllDesks(Pageable pageable) {
        return deskRepository.findAll(pageable).map(DeskMapper.INSTANCE::toDomain);
    }
}
