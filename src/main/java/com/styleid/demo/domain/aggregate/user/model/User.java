package com.styleid.demo.domain.aggregate.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.styleid.demo.application.config.Constants;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.*;

@Getter
@EqualsAndHashCode
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    private String password;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @NotNull
    private boolean activated;

    @Size(min = 2, max = 10)
    private String langKey;

    @Size(max = 256)
    private String imageUrl;

    @Size(max = 20)
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @JsonIgnore
    private String resetKey;

    @Column(name = "reset_date")
    private Instant resetDate;

    @JsonIgnore
    @Builder.Default
    private Set<Authority> authorities = new HashSet<>();

    @JsonIgnore
    private String createdBy;

    @JsonIgnore
    @Builder.Default
    private Instant createdDate = Instant.now();

    @JsonIgnore
    private String lastModifiedBy;

    @JsonIgnore
    @Builder.Default
    private Instant lastModifiedDate = Instant.now();

    @JsonIgnore
    public void activate() {
        this.activated = true;
    }
}
