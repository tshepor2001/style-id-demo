package com.styleid.demo.domain.aggregate.booking.desk.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.styleid.demo.application.config.Constants;
import com.styleid.demo.domain.aggregate.user.model.Authority;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.*;

@Getter
@EqualsAndHashCode
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Desk implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
    private Integer monitors;

    private DockingStationType dockingStationType;
}
