package com.styleid.demo.domain.aggregate.user.service;

import com.styleid.demo.application.config.Constants;
import com.styleid.demo.application.dto.AdminUserDTO;
import com.styleid.demo.application.dto.UserDTO;
import com.styleid.demo.application.security.AuthoritiesConstants;
import com.styleid.demo.application.security.SecurityUtils;
import com.styleid.demo.domain.aggregate.user.model.Authority;
import com.styleid.demo.domain.aggregate.user.model.User;
import com.styleid.demo.domain.service.EmailAlreadyUsedException;
import com.styleid.demo.domain.service.InvalidPasswordException;
import com.styleid.demo.domain.service.UsernameAlreadyUsedException;
import com.styleid.demo.infrastructure.repository.user.AuthorityRepository;
import com.styleid.demo.infrastructure.repository.user.UserRepository;
import com.styleid.demo.infrastructure.repository.user.mapper.AuthorityMapper;
import com.styleid.demo.infrastructure.repository.user.mapper.UserMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.security.RandomUtil;

@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository
            .findOneByActivationKey(key)
            .map(
                user -> {
                    // activate given user for the registration key.
                    user.setActivated(true);
                    user.setActivationKey(null);
                    log.debug("Activated user: {}", user);
                    return user;
                }
            )
            .map(UserMapper.INSTANCE::toDomain);
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository
            .findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(
                user -> {
                    user.setPassword(passwordEncoder.encode(newPassword));
                    user.setResetKey(null);
                    user.setResetDate(null);
                    return user;
                }
            )
            .map(UserMapper.INSTANCE::toDomain);
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository
            .findOneByEmailIgnoreCase(mail)
            .filter(com.styleid.demo.infrastructure.repository.user.model.User::isActivated)
            .map(
                user -> {
                    user.setResetKey(RandomUtil.generateResetKey());
                    user.setResetDate(Instant.now());
                    return user;
                }
            )
            .map(UserMapper.INSTANCE::toDomain);
    }

    public User registerUser(AdminUserDTO userDTO, String password) {
        userRepository
            .findOneByLogin(userDTO.getLogin().toLowerCase())
            .map(UserMapper.INSTANCE::toDomain)
            .ifPresent(
                existingUser -> {
                    boolean removed = removeNonActivatedUser(existingUser);
                    if (!removed) {
                        throw new UsernameAlreadyUsedException();
                    }
                }
            );

        userRepository
            .findOneByEmailIgnoreCase(userDTO.getEmail())
            .map(UserMapper.INSTANCE::toDomain)
            .ifPresent(
                existingUser -> {
                    boolean removed = removeNonActivatedUser(existingUser);
                    if (!removed) {
                        throw new EmailAlreadyUsedException();
                    }
                }
            );

        User savedUser = saveUser(
            User
                .builder()
                .login(userDTO.getLogin().toLowerCase())
                .password(passwordEncoder.encode(password))
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .email(Optional.ofNullable(userDTO.getEmail()).map(String::toLowerCase).orElse(null))
                .imageUrl(userDTO.getImageUrl())
                .langKey(userDTO.getLangKey())
                .activated(false)
                .activationKey(RandomUtil.generateActivationKey())
                .authorities(new HashSet<>(Collections.singletonList(Authority.builder().name(AuthoritiesConstants.USER).build())))
                .build()
        );
        log.debug("Created Information for User: {}", savedUser);
        return savedUser;
    }

    private User saveUser(User user) {
        return UserMapper.INSTANCE.toDomain(userRepository.save(UserMapper.INSTANCE.fromDomain(user)));
    }

    private boolean removeNonActivatedUser(User existingUser) {
        if (existingUser.isActivated()) {
            return false;
        }
        userRepository.delete(UserMapper.INSTANCE.fromDomain(existingUser));
        userRepository.flush();
        return true;
    }

    public User createUser(AdminUserDTO userDTO) {
        User savedUser = saveUser(
            User
                .builder()
                .login(userDTO.getLogin().toLowerCase())
                .password(passwordEncoder.encode(RandomUtil.generatePassword()))
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .email(Optional.ofNullable(userDTO.getEmail()).map(String::toLowerCase).orElse(null))
                .imageUrl(userDTO.getImageUrl())
                .langKey(Optional.ofNullable(userDTO.getLangKey()).orElse(Constants.DEFAULT_LANGUAGE))
                .resetKey(RandomUtil.generateResetKey())
                .resetDate(Instant.now())
                .activated(true)
                .activationKey(RandomUtil.generateActivationKey())
                .authorities(authorities(userDTO.getAuthorities()))
                .build()
        );
        log.debug("Created Information for User: {}", savedUser);
        return savedUser;
    }

    private Set<Authority> authorities(Set<String> authorities) {
        return authorities.stream().map(a -> Authority.builder().name(a).build()).collect(Collectors.toSet());
    }

    public Optional<AdminUserDTO> updateUser(AdminUserDTO userDTO) {
        return Optional
            .of(userRepository.findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(
                user -> {
                    user.setLogin(userDTO.getLogin().toLowerCase());
                    user.setFirstName(userDTO.getFirstName());
                    user.setLastName(userDTO.getLastName());
                    if (userDTO.getEmail() != null) {
                        user.setEmail(userDTO.getEmail().toLowerCase());
                    }
                    user.setImageUrl(userDTO.getImageUrl());
                    user.setActivated(userDTO.isActivated());
                    user.setLangKey(userDTO.getLangKey());
                    Set<com.styleid.demo.infrastructure.repository.user.model.Authority> managedAuthorities = user.getAuthorities();
                    managedAuthorities.clear();
                    userDTO
                        .getAuthorities()
                        .stream()
                        .map(authorityRepository::findById)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(managedAuthorities::add);
                    log.debug("Changed Information for User: {}", user);
                    return user;
                }
            )
            .map(UserMapper.INSTANCE::toDomain)
            .map(AdminUserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository
            .findOneByLogin(login)
            .ifPresent(
                user -> {
                    userRepository.delete(user);
                    log.debug("Deleted User: {}", user);
                }
            );
    }

    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(
                user -> {
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    if (email != null) {
                        user.setEmail(email.toLowerCase());
                    }
                    user.setLangKey(langKey);
                    user.setImageUrl(imageUrl);
                    log.debug("Changed Information for User: {}", user);
                }
            );
    }

    @Transactional
    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(
                user -> {
                    String currentEncryptedPassword = user.getPassword();
                    if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                        throw new InvalidPasswordException();
                    }
                    String encryptedPassword = passwordEncoder.encode(newPassword);
                    user.setPassword(encryptedPassword);
                    log.debug("Changed password for User: {}", user);
                }
            );
    }

    @Transactional(readOnly = true)
    public Page<AdminUserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAll(pageable).map(UserMapper.INSTANCE::toDomain).map(AdminUserDTO::new);
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllPublicUsers(Pageable pageable) {
        return userRepository.findAllByIdNotNullAndActivatedIsTrue(pageable).map(UserMapper.INSTANCE::toDomain).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login).map(UserMapper.INSTANCE::toDomain);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneWithAuthoritiesByLogin)
            .map(UserMapper.INSTANCE::toDomain);
    }

    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(
                user -> {
                    log.debug("Deleting not activated user {}", user.getLogin());
                    userRepository.delete(user);
                }
            );
    }

    @Transactional(readOnly = true)
    public List<String> getAuthorities() {
        return authorityRepository
            .findAll()
            .stream()
            .map(com.styleid.demo.infrastructure.repository.user.model.Authority::getName)
            .collect(Collectors.toList());
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findOneWithAuthoritiesByEmailIgnoreCase(email).map(UserMapper.INSTANCE::toDomain);
    }

    public Optional<User> findOneByLogin(String userLogin) {
        return userRepository.findOneByLogin(userLogin).map(UserMapper.INSTANCE::toDomain);
    }

    public User save(User user) {
        return UserMapper.INSTANCE.toDomain(userRepository.save(UserMapper.INSTANCE.fromDomain(user)));
    }

    public Optional<User> findOneWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login).map(UserMapper.INSTANCE::toDomain);
    }

    public User saveAndFlush(User user) {
        return UserMapper.INSTANCE.toDomain(userRepository.saveAndFlush(UserMapper.INSTANCE.fromDomain(user)));
    }

    public Optional<Authority> findAuthorityById(String id) {
        return authorityRepository.findById(id).map(AuthorityMapper.INSTANCE::toDomain);
    }

    public Optional<User> findOneWithAuthoritiesByEmailIgnoreCase(String login) {
        return userRepository.findOneWithAuthoritiesByEmailIgnoreCase(login).map(UserMapper.INSTANCE::toDomain);
    }

    public void delete(User user) {
        userRepository.delete(UserMapper.INSTANCE.fromDomain(user));
    }

    public List<User> findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant date) {
        return userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(date)
            .stream()
            .map(UserMapper.INSTANCE::toDomain)
            .collect(Collectors.toList());
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id).map(UserMapper.INSTANCE::toDomain);
    }
}
