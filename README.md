# styleid demo (very simplistic hot-desk booking system)

TECH:

Scaffolding tool: Jhipster

FE: Angular 12

BE:
DB: MySQL (H2 for dev)
Migration using Liquibase

ORM: Hibernate

MapStruct

Lombok

CI/CD:

GitLab CI

Docker

Docker-compose

Application design (considerations):

Business logic resides in the service layer. The FE should have as little business logic as possible.

3 layers

- Application
  - exposes rest api
  - application & security configuration
- Domain
  - Business logic (Rich domain model with a service per aggregate for orchestration)
  - Aggregate service -> Entry point into the domain & calls infrastructure layer to retrieve/perist data by calling repositories
  - Decouple business logic from infrastructure concerns (database persistence)
- Infrastructure
  - Database persistence logic \* entities

Run the application:

```
./mvnw
```

Docker

```
./mvnw -Pprod verify jib:dockerBuild
```

Then run:

```
docker-compose -f src/main/docker/app.yml up -d
```
